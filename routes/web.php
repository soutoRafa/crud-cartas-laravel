<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
})->name('index');

Route::group(['prefix' => 'admin'], function (){
	Route::get('Cadastro', 'UsersController@Cadastro');
	Route::post('Cadastrar', 'UsersController@Cadastrar');
});

Route::get('/cards', 'CardsController@CardList');

Auth::routes();
Route::get('/logout', 'UsersController@LogOut');
Route::get('/home', 'UsersController@home')->name('home');
Route::post('/destroy/{id}', 'UsersController@destroy')->name('destroy')->middleware('auth');
Route::get('/atualizar/{id}/', 'UsersController@atualizarUsuario');
Route::post('/salvar/{id}/', 'UsersController@salvarAlteracoes');
Route::post('/upload', 'UsersController@Move');