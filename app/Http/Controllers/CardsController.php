<?php

namespace Cartas\Http\Controllers;

use Illuminate\Http\Request;
use Cartas\Cards;

class CardsController extends Controller
{
	
	public function CardList()
	{
		$cards = Cards::all();

    	return view('cartas')->with('c', $cards);
	}
    
}
