<?php

namespace Cartas\Http\Controllers;

use Cartas\User;
use Request;
use Illuminate\Support\Facades\Validator;
use Cartas\Http\Requests\CadastroRequest;
use Cartas\Http\Requests\UpdateRequest;
use Session;
use Illuminate\Support\Facades\Auth;

class UsersController extends Controller
{
	public function __construct(Request $request)
	{
		
 		$this->middleware('auth');
	}
   public function index()
	{
		return view('index');
	}
	public function Cadastro()
	{
		return view('cadastro');
	}
	public function Cadastrar(CadastroRequest $request)
	{
		
		$user = new User();
		$user->name = Request::input('nome');
		$user->email = Request::input('email');
		$senha = Request::input('senha');
		$user->password = bcrypt($senha);

		$user->save();

		$request->session()->flash('status', 'Cadastro realizado com sucesso!');

		return redirect('/');

	}
	public function home(Request $request)
	{
		$user = Auth::user();
		$allusers = User::paginate(5);
		return view('home')->with('user', $user)->with('allusers', $allusers);
	}

	public function atualizarUsuario(){
		$id = Request::route('id');
		$user = User::find($id);

		return view('atualizar')->with('user', $user);
	}
	
	public function salvarAlteracoes(UpdateRequest $request){
		$id = Request::route('id');
		
		$user = User::find($id);
		$user->name = Request::input('nome');
		$user->email = Request::input('email');
		$user->password = Request::input('senha');

		$user->save();
		
		$request->session()->flash('update', 'Atualização realizada com sucesso!');


		return back();
	}

	public function Move(){
	    if (Request::hasFile('photo')){

	        echo 'Uploaded';
	        $file=Request::file('photo');
	        $file->store('imgs/' , $file->getClientOriginalName());
	        echo '<img src="imgs/' . $file->getClientOriginalName().'"/>';     
	    }

	    else {

	        echo 'Not Uploaded';
	    }
}

	public function destroy(Request $request){
		$id = Request::route('id');
		
		User::destroy($id);
		
		return redirect('/home');
	}

	public function LogOut()
	{
		Auth::logout();

		return redirect('/');
	}
	
}
