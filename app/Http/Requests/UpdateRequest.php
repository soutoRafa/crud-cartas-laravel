<?php

namespace Cartas\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = auth::id();
        
        return [
            'nome' => 'required|min:3|max:50',
            'email' => 'required|min:5|max:100|unique:users,id,'.$id,
            'senha' => 'required|min:3',
            'senha_confirmation' => 'required|min:3|same:senha',   
        ];
    }
     public function messages()
    {
        return[
            'nome.required' => 'O campo nome é obrigatório',
            'email.required' => 'O campo email é obrigatório',
            'senha.required' => 'O campo senha é obrigatório',
            'senha_confirmation.required' => 'O campo repetir senha é obrigatório',
            'nome.min' => 'O campo nome deve ter pelo menos 3 caracteres',
            'email.min' => 'O campo email deve ter pelo menos 5 caracteres',
            'senha.min' => 'O campo senha deve ter pelo menos 3 caracteres',
            'email.unique' => 'O campo email deve ser único',
            'senha_confirmation.same' => 'O campo nova senha e repetir nova senha devem ser iguais',
        ];
    }    
}
