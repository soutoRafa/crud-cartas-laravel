<!DOCTYPE html>
<html>
<head>
	<title>@yield('title')</title>
	<link rel="stylesheet" type="text/css" href="/css/app.css">
	<link rel="stylesheet" type="text/css" href="/css/estilos.css">
</head>
<body>
<div class="container">
	@yield('conteudo')
</div>

</body>
</html>