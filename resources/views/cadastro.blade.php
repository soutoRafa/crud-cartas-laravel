@extends('template')

@section('title')
	Cadastro administrador
@stop

@section('conteudo')
	<div class="col-md-offset-3">
		<h1>Bem vindo ao PokeTrade!</h1>
		<h3>Cadastre seu perfil de administrador</h3>
	
	</div>
	<div class="col-md-offset-3 login-box">
		@if( count( $errors->all() ) > 0 )
			<div class="alert alert-danger">
			@foreach($errors->all() as $error)
				<p>{{ $error }} </p>
			@endforeach	
			</div>
		@endif
		<form method="post" action="/admin/Cadastrar">
			
			<input value="{{ csrf_token() }}" name="_token" type="hidden">

			<div class="form-group">
				<label for="nome"> Nome </label>
				<input type="text" name="nome" class="form-control" id="nome">
			</div>

			<div class="form-group">
				<label for="email"> Email </label>
				<input type="text" name="email" class="form-control" id="email">
			</div>

			<div class="form-group">
				<label for="senha"> Senha </label>
				<input type="password" name="senha" class="form-control" id="senha">
			</div>

			<button type="submit" class="btn btn-primary">
			
			Cadastrar
			
			</button>

		</form>
		<p>Já possui conta <a href="/">faça seu login aqui</a></p>
	</div>	
@stop