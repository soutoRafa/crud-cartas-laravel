<!DOCTYPE html>
<html>
<head>
	<title>@yield('title')</title>
	<link rel="stylesheet" type="text/css" href="/css/app.css">
	<link rel="stylesheet" type="text/css" href="/css/dashboard.css">
</head>
<body>
<div class="navbar navbar-dark bg-primary">
    <div class="navbar-brand">
        <div id="titulo-logo"> Gerenciador de cartas </div>
    </div>
    <div class="nav navbar-nav navbar-right menu">    
        <ul>
            <li><a href="">lorem</a></li>
            <li><a href="">lorem</a></li>
            <li><a href="">lorem</a></li>
            <li><a href="">lorem</a></li>
            <li><a href="{{ route('logout') }}">LogOut</a></li>
        </ul>
    </div>    
</div>

	@yield('conteudo')
	
	<script src="{{asset('/js/app.js')}}" ></script>
	
	<script type="text/javascript">
   /* $(document).ready(function() {
        $('.btn-delete').click( function() {
            var id = $(this).val();
            var token = $('#token').val();      
          
            $.ajax({
                url: "/destroy",
                type: 'post',
                data:{ id: id, _token: token}

            }); 
        });
    });    */
</script>
</body>
</html>