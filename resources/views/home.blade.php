@extends('site.template')
@section('title')
Home
@endsection
@section('conteudo')

<div class="col-md-4">
    <div class="infos-perfil">
        <h1>{{$user->name}}</h1>
        <img src="{{ asset('imgs/default.png') }}" class="img-circle imagem-perfil">
        <p class="adm">Administrador</p>

        <p class="adm">{{$user->email}}</p>
        
    </div>
</div>
<div class="col-md-8">
    <h1>Lista de usuários</h1>
 
    <table class="table table-striped table-hover">
        <thead class="thead-inverse">
            <th>id</th>
            <th>Nome</th>
            <th>Email</th>
            <th>Editar</th>
            <th>Deletar</th>
        </thead>
        @foreach($allusers as $all)   
        <tbody class="id{{ $all->id }}">
            <td>{{$all->id}}</td>
            <td>{{$all->name}}</td>
            <td>{{$all->email}}</td>
            <th>
                <form action="/atualizar/{{$all->id}}">
                    <button class="btn btn-primary btn-edit">Editar</button>
                </form>
            </th>
            <th>
                <form action="/destroy/{{$all->id}}" method="post">
                    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                    <button class="btn btn-danger btn-delete" value="{{$all->id}}">Deletar</button>
                </form>    
            </th>
        </tbody>
        @endforeach
    </table>
    {{$allusers->links()}}    

</div>

@endsection
