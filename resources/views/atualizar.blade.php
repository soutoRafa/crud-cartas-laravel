@extends('site.template')

@section('title')
	Editar perfil
@endsection

@section('conteudo')
<div class="container">

	<div class="col-md-6">
    	<div class="infos-perfil">
        	<h1>{{$user->name}}</h1>
        	<img src="{{ asset('imgs/default.png') }}" class="img-circle imagem-perfil">
        	<p class="adm">Cliente</p>
        	
        	<form action="/upload" method="post" enctype="multipart/form-data">
        		<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
        		<input type="file" name="photo">
        		
        		<button class="btn btn-default" style="margin: 30px 0 0 30px;">Atualizar foto</button>  
    		
    		</form>
    	</div>
	</div>

<div class="col-md-6">
    <h1>Atualizar perfil do usuário</h1>    
     		@if(Session::has('update'))
        		<div class="alert alert-success"> 
        			{{ Session('update') }} 
        		</div>
        	@endif

        	@if( count( $errors->all() ) > 0 )
			<div class="alert alert-danger">
				@foreach($errors->all() as $error)
					<p>{{ $error }} </p>
				@endforeach	
			</div>
			@endif
			
        <form action="/salvar/{{$user->id}}" method="post">
        	
        	<div class="form-group">
        		<label>Nome</label>
        		<input type="text" name="nome" class="form-control" value="{{$user->name}}">
        	</div>
        	<div class="form-group">
        		<label>Email</label>
        		<input type="text" name="email" class="form-control" value="{{$user->email}}">
        	</div>
        	<div class="form-group">
        		<label>Nova senha</label>
        		<input type="password" name="senha" class="form-control" placeholder="Nova senha">
        	</div>

        	<div class="form-group">
        		<label>Repetir nova senha</label>
        		<input type="password" name="senha_confirmation" class="form-control" placeholder="Repetir nova senha">
        	</div>

        	<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

        	<button class="btn btn-primary">Atualizar</button>

        </form>
</div>

</div>
@endsection