@extends('template')

@section('title')
	Administrador PKT
@stop

@section('conteudo')
<div class="conteudo">
	<div class="col-md-12">
		<h1>Bem vindo ao PokeTrade!</h1>
		<h3>Faça login para continuar</h3>
		
	
	<div class="login-box">
		@if( Session::has('status') )
			<div class="alert alert-success">{{Session('status')}}</div>
		@endif
        @if( Session::has('authfail') )
            <div class="alert alert-danger">{{Session('authfail')}}</div>
        @endif
		<form method="post" method="POST" action="{{ route('login') }}">
            {{ csrf_field() }}
			<div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
				<label for="login"> Email </label>
				<input type="email" name="email" class="form-control" id="email">
				@if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
			</div>
			<div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
				<label for="senha"> Senha </label>
				<input type="password" name="password" class="form-control" id="senha">
 				
 				@if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif

			</div>
			<button type="submit" class="btn btn-primary">Entrar</button>
		</form>
		<p>Ou <a href="/admin/Cadastro">cadastre-se aqui</a></p>
	</div>
	</div>
</div>	
@stop


                        <!--<div class="form-group">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Login
                                </button>**>-->